/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LNN_LANE_DFX_H
#define LNN_LANE_DFX_H

#include <stdint.h>
#include "lnn_lane_interface.h"
#include "lnn_lane_link_p2p.h"
#include "lnn_event.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    EVENT_LANE_STAGE,
    EVENT_RESULT_CODE,
    EVENT_LANE_ID,
    EVENT_LANE_TYPE,
    EVENT_LOCAL_CAP,
    EVENT_REMOTE_CAP,
    EVENT_ONLINE_STATE,
    EVENT_GUIDE_TYPE,
    EVENT_LINK_RETRY,
    EVENT_GUIDE_RETRY,
    EVENT_HML_REUSE,
    EVENT_BUILD_LINK_TIME,
    EVENT_WIFI_DETECT_TIME,
    EVENT_WIFI_DETECT_STATE,
    EVENT_FREE_LINK_TIME,
    EVENT_DELAY_FREE,
    EVENT_BUTT,
} LaneEventType;

typedef enum {
    WIFI_DETECT_NO_START,
    WIFI_DETECT_SUCC,
    WIFI_DETECT_FAIL,
    WIFI_DETECT_BUTT,
} WifiDetectState;

typedef struct {
    ListNode node;
    LnnEventLaneStage stage;
    uint32_t retCode;
    uint32_t laneHandle;
    uint32_t minBW;
    uint32_t maxLaneLatency;
    uint32_t minLaneLatency;
    uint32_t rttLevel;
    char peerNetWorkId[NETWORK_ID_BUF_LEN];
    LaneTransType transType;
    uint64_t laneId;
    LaneLinkType linkType;
    uint32_t localDynamicCap;
    uint32_t localStaticCap;
    uint32_t remoteDynamicCap;
    uint32_t remoteStaticCap;
    uint32_t onlineType;
    WdGuideType guideType;
    bool isLinkRetry;
    bool isGuideRetry;
    WifiDetectState wifiDetectState;
    uint64_t buildLinkTime;
    uint64_t wifiDetectTime;
    bool isHmlReuse;
    bool isDelayFree;
    uint64_t freeLinkTime;
} LaneProcess;

int32_t CreateLaneEventInfo(LaneProcess *processInfo);
int32_t UpdateLaneEventInfo(uint32_t laneHandle, LaneEventType eventType, void *arg);
int32_t ReportLaneEventInfo(uint32_t laneHandle);
int32_t InitLaneEvent(void);

#ifdef __cplusplus
}
#endif
#endif // LNN_LANE_DFX_H