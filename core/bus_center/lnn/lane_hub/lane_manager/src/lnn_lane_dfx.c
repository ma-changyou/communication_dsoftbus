/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lnn_lane_dfx.h"

#include "securec.h"
#include "softbus_error_code.h"
#include "softbus_adapter_mem.h"
#include "softbus_def.h"
#include "lnn_log.h"

typedef enum {
    DECIDE_LINK_STAGE,
    BUILD_LINK_STAGE,
    DESTROY_LINK_STAGE,
} LaneProcessType;

static SoftBusList g_laneProcessInfo;

static int32_t LaneEventLock(void)
{
    return SoftBusMutexLock(&g_laneProcessInfo.lock);
}

static void LaneEventUnLock(void)
{
    (void)SoftBusMutexUnlock(&g_laneProcessInfo.lock);
}

static LaneProcess* GetLaneEventWithoutLock(uint32_t laneHandle)
{
    LaneProcess *laneProcess = NULL;
    LaneProcess *next = NULL;
    if (laneHandle == INVALID_LANE_REQ_ID) {
        LNN_LOGE(LNN_LANE, "laneHandle is invalid parameter");
        return NULL;
    }
    LIST_FOR_EACH_ENTRY_SAFE(laneProcess, next, &g_laneProcessInfo.list, LaneProcess, node) {
        if (laneProcess->laneHandle == laneHandle) {
            LNN_LOGI(LNN_LANE, "get laneProcess laneHandle = %{public}u", laneHandle);   
            return laneProcess;
        }
    }
    return NULL;
}

static int32_t UpdateInfoWithEventType(LaneProcess *info, LaneEventType eventType, void *arg)
{
    switch (eventType) {
        case EVENT_RESULT_CODE:
            info->retCode = *((uint32_t *)arg);
            break;
        case EVENT_LANE_STAGE:
            info->stage = *((uint32_t *)arg);
            break;
        case EVENT_LANE_TYPE:
            info->linkType = *((uint32_t *)arg);
            break;
        case EVENT_LANE_ID:
            info->laneId = *((uint32_t *)arg);
            break;
        case EVENT_LOCAL_CAP:
            info->localDynamicCap = *((uint32_t *)arg);
            break;
        case EVENT_REMOTE_CAP:
            info->remoteDynamicCap = *((uint32_t *)arg);
            break;
        case EVENT_ONLINE_STATE:
            info->onlineType = *((uint32_t *)arg);
            break;
        case EVENT_GUIDE_TYPE:
            info->guideType = *((uint32_t *)arg);
            break;
        case EVENT_GUIDE_RETRY:
            info->isGuideRetry = *((uint32_t *)arg);
            break;
        case EVENT_LINK_RETRY:
            info->isLinkRetry = *((uint32_t *)arg);
            break;
        case EVENT_HML_REUSE:
            info->isHmlReuse = *((uint32_t *)arg);
            break;
        case EVENT_BUILD_LINK_TIME:
            info->buildLinkTime = *((uint32_t *)arg);
            break;
        case EVENT_WIFI_DETECT_TIME:
            info->wifiDetectTime = *((uint32_t *)arg);
            break;
        case EVENT_WIFI_DETECT_STATE:
            info->wifiDetectState = *((uint32_t *)arg);
            break;
        case EVENT_FREE_LINK_TIME:
            info->freeLinkTime = *((uint32_t *)arg);
            break;
        case EVENT_DELAY_FREE:
            info->isDelayFree = *((uint32_t *)arg);
            break;
        default:
            return SOFTBUS_INVALID_PARAM; 
    }
    return SOFTBUS_OK;
}

int32_t CreateLaneEventInfo(LaneProcess *processInfo)
{
    if (processInfo == NULL) {
        LNN_LOGE(LNN_LANE, "param processInfo is null");
        return SOFTBUS_INVALID_PARAM;
    }
    LaneProcess *info = (LaneProcess *)SoftBusCalloc(sizeof(LaneProcess));
    if (info == NULL) {
        LNN_LOGE(LNN_LANE, "processInfo is null");
        return SOFTBUS_MALLOC_ERR;
    }
    if (memcpy_s(info, sizeof(LaneProcess), processInfo, sizeof(LaneProcess)) != EOK) {
        LNN_LOGE(LNN_LANE, "copy processInfo fail");
        return SOFTBUS_MEM_ERR;
    }
    if (LaneEventLock() != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "lane lock fail");
        SoftBusFree(info);
        return SOFTBUS_LOCK_ERR;
    }
    ListAdd(&g_laneProcessInfo.list, &info->node);
    g_laneProcessInfo.cnt++;
    LaneEventUnLock();
    return SOFTBUS_OK;
}

static int32_t DeleteLaneEventInfo(uint32_t laneHandle)
{
    if (LaneEventLock() != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "lane lock fail");
        return SOFTBUS_LOCK_ERR;
    }
    LaneProcess *next = NULL;
    LaneProcess *item = NULL;
    LIST_FOR_EACH_ENTRY_SAFE(item, next, &g_laneProcessInfo.list, LaneProcess, node) {
        if (item->laneHandle == laneHandle) {
            ListDelete(&item->node);
            SoftBusFree(item);
            g_laneProcessInfo.cnt--;
            LaneEventUnLock();
            return SOFTBUS_OK;
        }
    }
    LaneEventUnLock();
    return SOFTBUS_LANE_NOT_FOUND;
}

int32_t UpdateLaneEventInfo(uint32_t laneHandle, LaneEventType eventType, void *arg)
{
    if (arg == NULL || eventType == EVENT_BUTT || laneHandle == INVALID_LANE_REQ_ID) {
        LNN_LOGE(LNN_LANE, "invalid parameter");
        return SOFTBUS_INVALID_PARAM;
    }
    if (LaneEventLock() != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "lane lock fail");
        return SOFTBUS_LOCK_ERR;
    }
    LaneProcess *info = GetLaneEventWithoutLock(laneHandle);
    if (info == NULL) {
        LNN_LOGE(LNN_LANE, "not found laneProcess info");
        LaneEventUnLock();
        return SOFTBUS_LANE_NOT_FOUND;
    }
    int32_t ret = UpdateInfoWithEventType(info, eventType, arg);
    if (ret != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "update laneProcess fail, laneHandle = %{public}u", laneHandle);
        LaneEventUnLock();
        return ret;
    }
    LaneEventUnLock();
    return ret;
}

int32_t ReportLaneEventInfo(uint32_t laneHandle)
{
    if (laneHandle == INVALID_LANE_REQ_ID) {
        LNN_LOGE(LNN_LANE, "laneHandle is invalid parameter");
        return SOFTBUS_INVALID_PARAM;
    }
    if (LaneEventLock() != SOFTBUS_OK) {
        LNN_LOGE(LNN_LANE, "lane lock fail");
        return SOFTBUS_LOCK_ERR;
    }
    LaneProcess *info = GetLaneEventWithoutLock(laneHandle);
    if (info == NULL) {
        LNN_LOGE(LNN_LANE, "not found laneProcess info");
        LaneEventUnLock();
        return SOFTBUS_LANE_NOT_FOUND;
    }
    LaneEventUnLock();
    LnnEventExtra extra = {
        .result = EVENT_STAGE_RESULT_OK,
        .errcode = (int32_t)info->retCode,
        .laneStage = (int32_t)info->stage,
        .laneHandle = (int32_t)info->laneHandle,
        .laneId = (int64_t)info->laneId,
        .laneLinkType = (int32_t)info->linkType,
        .minBW = (int32_t)info->minBW,
        .maxLaneLatency = (int32_t)info->maxLaneLatency,
        .minLaneLatency = (int32_t)info->minLaneLatency,
        .rttLevel = (int32_t)info->rttLevel,
        .peerNetworkId = (const char *)info->peerNetWorkId,
        .transType = (int32_t)info->transType,
        .localDynamicCap = (int32_t)info->localDynamicCap,
        .remoteDynamicCap = (int32_t)info->remoteDynamicCap,
        .onlineType = (int32_t)info->onlineType,
        .guideType = (int32_t)info->guideType,
        .isGuideRetry = (int32_t)info->isGuideRetry,
        .isLinkRetry = (int32_t)info->isLinkRetry,
        .wifiDetectState = (int32_t)info->wifiDetectState,
        .wifiDetectTime = (int64_t)info->wifiDetectTime,
        .buildLinkTime = (int64_t)info->buildLinkTime,
        .isHmlReuse = (int32_t)info->isHmlReuse,
        .isDelayFree = (int32_t)info->isDelayFree,
        .freeLinkTime = (int64_t)info->freeLinkTime,
    };
    LNN_EVENT(EVENT_SCENE_LNN, EVENT_STAGE_LNN_LANE_SELECT_END, extra);
    return DeleteLaneEventInfo(laneHandle);
}

int32_t InitLaneEvent(void)
{
    if(SoftBusMutexInit(&g_laneProcessInfo.lock, NULL) != SOFTBUS_OK) {
        LNN_LOGI(LNN_LANE, "lane resource mutex init fail");
        return SOFTBUS_NO_INIT;
    }
    ListInit(&g_laneProcessInfo.list);
    g_laneProcessInfo.cnt = 0;
    return SOFTBUS_OK;
}

